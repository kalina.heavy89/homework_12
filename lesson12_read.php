<?php

define("ROOT_PATH", dirname(__FILE__));

$usersFile = fopen(ROOT_PATH . DIRECTORY_SEPARATOR . "users.txt", "r"); //Reading file users.txt
$lineArray = [];
$users = [];
$i = 0;

if ($usersFile) {
    while (($usersString = fgets($usersFile, 4096)) !== false) {
        //Deleting line translation carriage return and other symbols
        $usersString = rtrim($usersString, " \t\n\r\0\x0B");
		$lineArray = explode (" ", $usersString);
		$users [$i]["name"] = $lineArray [0];
		$users [$i]["login"] = $lineArray [1];
		$users [$i]["password"] = $lineArray [2];
		$users [$i]["email"] = $lineArray [3];
		$users [$i]["language"] = $lineArray [4];
        $i++;
    }
    if (!feof($usersFile)) {
        echo "Error: fgets() suddenly failed<br>";
        die();
    }
    fclose($usersFile);
}

/*echo "<pre>";
var_dump ($users);
echo "</pre>";*/